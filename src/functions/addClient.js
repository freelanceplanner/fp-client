'use strict'

module.exports.addClient = async (event) => {
  let bodyObj = {}
  try {

  } catch (jsonError) {
    console.log('There was an error parsing the JSON', jsonError)
    console.log('event.body', event.body)
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Headers': 'Authorization'
      }
    }
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Success!'
    })
  }
}
